# bonchTGBridge
The better interface for all the bonch website related things

The project is currently in dire need of a rewrite :)

## Usage
### Manual
0) Clone the project:
```shell
git clone https://gitlab.com/vinyl_summer/bonchtgbridge.git && cd bonchtgbridge
```
1) Create and activate a new virtual environment
``` shell
python -m venv /path/to/new/virtual/environment
```
``` shell
source <new_venv_path>/bin/activate
```
2) Install the project requirements
```shell
pip install -r requirements.txt
```
3) Set the following environment variables:
```shell
export API_TOKEN=<your TG bot api token, mandatory>
```
```shell
export DB_PATH=<path to the database file, mandatory>
```
4) Run the app:
```shell
python -m bonchtgbridge
```

### Docker
Build the image yourself:
```shell
git clone https://gitlab.com/vinyl_summer/suskabot.git && cd suskabot
```
```shell
docker build -t bonchtgbridge .
```
```shell
docker run -d --env=API_TOKEN=<TG bot api token> --env=DB_URL=<db file path> bonchtgbridge
```

or use one from the gitlab registry:
```shell
docker run -d --env=API_TOKEN=<TG bot api token> --env=DB_URL=<db file path> registry.gitlab.com/vinyl_summer/bonchtgbridge
```
