import logging
import os

from bonchtgbridge.app.bot import start_bot


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO,
    force=True
)
logging.getLogger("httpx").setLevel(logging.WARNING)


assert 'API_TOKEN' in os.environ, "You must set Telegram API token environment variable"


start_bot(os.environ['API_TOKEN'])
