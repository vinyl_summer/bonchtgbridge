import os

from peewee import (SqliteDatabase, Model,
                    TextField, DateTimeField,
                    ForeignKeyField, BooleanField)

assert 'DB_PATH' in os.environ
db = SqliteDatabase(os.environ['DB_PATH'])


class BaseModel(Model):
    class Meta:
        database = db


# one bonch user can have multiple TG accounts,
# but I'm not sure if I want multiple TG users to have access to one bonch account
class User(BaseModel):
    tg_id = TextField(unique=True)

    full_name = TextField(null=True)
    login = TextField(null=True)
    password = TextField(null=True)

    initial_setup_status = TextField(
        choices=[
            ('pending', 'the initial setup was not complete'),
            ('auth_exception', 'invalid login data exception'),
            ('timeout_exception', 'webdriver timeout'),
            ('just_finished', 'the initial setup is finished, but the user is not notified'),
            ('done', 'the initial setup was successfully finished')
        ],
        default='pending'
    )

    INITIAL_SETUP_PENDING = 'pending'
    INITIAL_SETUP_AUTH_EXCEPTION = 'auth_exception'
    INITIAL_SETUP_TIMEOUT_EXCEPTION = 'timeout_exception'
    INITIAL_SETUP_JUST_FINISHED = 'just_finished'
    INITIAL_SETUP_DONE = 'done'


class Content(BaseModel):
    type = TextField(
        choices=[
            ('message', 'a record that was originally a bonch message'),
            ('file', 'a record that was originally a bonch file'),
        ]
    )
    date_time = DateTimeField()
    topic = TextField()
    text = TextField()
    files = TextField()
    sender = TextField()
    recipient = ForeignKeyField(User, on_delete='CASCADE')
    is_read = BooleanField(default=False)


def init_db() -> None:
    with db:
        db.pragma('foreign_keys', 1, permanent=True)
        db.create_tables([User, Content])
