import logging

from datetime import datetime, timedelta

import peewee

from telegram import Update
from telegram.constants import ParseMode
from telegram.ext import Application, CommandHandler, ContextTypes, ConversationHandler, \
    MessageHandler, filters, CallbackQueryHandler, JobQueue, CallbackContext

from bonchtgbridge.models.models import User, Content, init_db
from bonchtgbridge.services.task_queue import WorkerPool
from bonchtgbridge.services.bonch_parser import InitialSetupTask, CheckNewMessages

from bonchtgbridge.utils.keyboard_constants import ITEMS_PER_PAGE, SHOW_RECENT_MESSAGES_LAST_WEEK_MARKUP, \
    RECENT_MESSAGES_PREVIOUS_BUTTON, RECENT_MESSAGES_NEXT_BUTTON, RECENT_MESSAGES_CURRENT_BUTTON, \
    SHOW_RECENT_MESSAGES_MARKUP, MESSAGES_PREVIOUS_BUTTON, MESSAGES_NEXT_BUTTON, MESSAGES_FIRST_BUTTON, \
    SHOW_MESSAGES_FIRST_PAGE_MARKUP, SHOW_MESSAGES_MARKUP, PRE_SETUP_MARKUP, CONFIRM_CREDENTIALS_MARKUP


logger = logging.getLogger(__name__)


PRE_SETUP, TYPING_CREDENTIALS, CREDENTIALS_SAVED, CONFIRMING = range(4)

CHECK_NEW_MESSAGES_INTERVAL_SECONDS = 60 * 30
NOTIFY_ABOUT_NEW_MESSAGES_INTERVAL_SECONDS = 60 * 5


class NewMessagesNotificationLoop:
    job_queue: JobQueue
    check_new_messages_interval: int
    notify_about_new_messages_interval: int
    people_to_notify_ids: list

    def __init__(
            self,
            job_queue: JobQueue,
            check_new_messages_interval: int,
            notify_about_new_messages_interval: int
    ):
        self.job_queue = job_queue
        self.check_new_messages_interval = check_new_messages_interval
        self.notify_about_new_messages_interval = notify_about_new_messages_interval
        self.people_to_notify_ids = []

    def add(self, user_id: int):
        self.people_to_notify_ids.append(user_id)

    def remove(self, user_id: int):
        self.people_to_notify_ids.remove(user_id)

    def _start_notification_loop(self):
        for user_id in self.people_to_notify_ids:
            user: User = User.get(tg_id=user_id)
            self.job_queue.run_repeating(
                callback=new_messages_notify,
                interval=self.notify_about_new_messages_interval,
                first=1,
                name=f"notify_new_messages_{user.full_name}",
                data=user_id
            )

    def _start_check_new_messages_loop(self):
        for user_id in self.people_to_notify_ids:
            user: User = User.get(tg_id=user_id)
            self.job_queue.run_repeating(
                callback=check_new_messages,
                interval=self.check_new_messages_interval,
                first=1,
                name=f"start_check_new_messages_{user.full_name}",
                data=user_id
            )

    def start(self):
        self._start_notification_loop()
        self._start_check_new_messages_loop()
        logger.info("Successfully started main new messages notification loop.")


async def initial_setup(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    worker_pool: WorkerPool = context.bot_data["worker_pool"]
    login: str = context.user_data['login']
    password: str = context.user_data['password']
    user: User = User.create(tg_id=update.effective_user.id)

    user.login = login
    user.password = password
    user.save()

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="The setup process has started! It can take a few minutes, "
             "depending on how many messages you have. You will be notified upon completion."
    )

    # TODO: catch potential QueueFull exception
    task = InitialSetupTask(user)
    worker_pool.add_task(task)

    context.job_queue.run_repeating(
        callback=initial_setup_notify,
        first=5,
        interval=10,
        chat_id=update.effective_chat.id,
        name=f"notify_{user.tg_id}",
        data=user.tg_id
    )


# TODO: rework the notification system. maybe add a on complete callback
# TODO: users should be asked for login data again, if there was an auth exception
async def initial_setup_notify(context: ContextTypes.DEFAULT_TYPE) -> None:
    notification_loop: NewMessagesNotificationLoop = context.bot_data["notification_loop"]
    user_tg_id: int = context.job.data
    chat_id: int = context.job.chat_id

    user: User = User.get(tg_id=user_tg_id)
    if user.initial_setup_status == User.INITIAL_SETUP_PENDING:
        return

    if user.initial_setup_status == User.INITIAL_SETUP_JUST_FINISHED:
        await context.bot.send_message(
            chat_id=chat_id,
            text=f"Welcome, {user.full_name}! The bot has been "
                 f"successfully set up. Use /help command to check "
                 f"all the available functionality"
        )
        user.initial_setup_status = user.INITIAL_SETUP_DONE
        user.save()

        notification_loop.add(user_tg_id)

    if user.initial_setup_status == user.INITIAL_SETUP_AUTH_EXCEPTION:
        await context.bot.send_message(
            chat_id=chat_id,
            text="Invalid login data!"
                 " Please, check the spelling and try again"
        )
        user.delete_instance()

    if user.initial_setup_status == User.INITIAL_SETUP_TIMEOUT_EXCEPTION:
        await context.bot.send_message(
            chat_id=chat_id,
            text="Looks like the bonch website is down :("
                 " Please, try again in a few minutes"
        )
        user.delete_instance()

    context.job.schedule_removal()


async def new_messages_notify(context: ContextTypes.DEFAULT_TYPE) -> None:
    user_tg_id: int = context.job.data
    user: User = User.get(tg_id=user_tg_id)
    unread_messages: list[Content] = list((Content.
                                          select().
                                          where(
                                            (Content.is_read == False) & # noqa E712 since peewee ORM is dogshit
                                            (Content.recipient == user)
                                          )))

    if len(unread_messages) > 0:
        response = ''
        for message in unread_messages:
            response += f"{format_user_message(message)}\n\n"
            message.is_read = True
            message.save()

        await context.bot.send_message(
            chat_id=user_tg_id,
            text=response,
            parse_mode=ParseMode.HTML
        )


def format_user_message(message: Content) -> str:
    formatted_files: list[str] = []
    for file in iter(message.files.splitlines()):
        formatted_files.append(f'<a href="{file}">{file.split("/")[-1]}</a>')
    files_string = '\n'.join(formatted_files)

    sender_full_name = message.sender.split()
    if len(sender_full_name) > 3:
        sender_full_name = sender_full_name[:-1]
    formatted_sender = f"{sender_full_name[0]} {sender_full_name[1][0]}. {sender_full_name[2][0]}."

    formatted_message = (f"{message.date_time.strftime('%d %B %Y %H:%M')} - {formatted_sender}\n"
                         f"<b>{message.topic}</b>\n"
                         f"{message.text}\n"
                         f"{files_string}")

    return formatted_message


async def get_user_messages_by_page(user_id: int, page: int) -> str:
    user = User.get(tg_id=user_id)
    user_messages = (Content.
                     select().
                     where(
                        Content.recipient == user
                     ).order_by(Content.date_time.desc())
                     .paginate(page, ITEMS_PER_PAGE)
                     )

    if not user_messages:
        return "No messages found!"

    messages_page_lines = [f"{index + 1}) {format_user_message(message)}"
                           for index, message in enumerate(user_messages)
                           ]
    return "\n\n".join(messages_page_lines)


async def get_user_messages_by_period(user_id, time_period) -> str:
    time_period_start, time_period_end = time_period
    user: User = User.get(tg_id=user_id)
    # TODO: implement caching system (and cache invalidation)
    user_messages = (Content.
                     select().
                     where(
                        Content.recipient == user,
                        Content.date_time >= time_period_start,
                        Content.date_time <= time_period_end
                     ).order_by(Content.date_time.desc())
                     )

    if not user_messages:
        return "No messages found!"

    return "\n\n".join([f"{format_user_message(message)}" for message in user_messages])


def get_current_week_start() -> datetime:
    current_date = datetime.now()
    start_of_week = current_date - timedelta(days=current_date.weekday())
    start_of_week = start_of_week.replace(hour=0, minute=0, second=0, microsecond=0)

    return start_of_week


# TODO: Fix possible MessageTooLong error. Pagination is an option
async def show_recent_messages(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user: User | None = User.get_or_none(tg_id=update.effective_user.id)
    if user is None or user.initial_setup_status != user.INITIAL_SETUP_DONE:
        await update.message.reply_text("You aren't logged in yet! Use /start or wait for your setup to complete")
        return
    context.user_data['current_messages_week_start'] = get_current_week_start()

    start_of_week: datetime = context.user_data['current_messages_week_start']
    end_of_week = start_of_week + timedelta(days=6)
    period = (start_of_week, end_of_week)

    user_id: int = update.effective_user.id
    user_messages = await get_user_messages_by_period(user_id, period)

    user_messages = f"{datetime.date(start_of_week)} — {datetime.date(end_of_week)}\n\n" + user_messages

    await update.message.reply_text(
        user_messages,
        reply_markup=SHOW_RECENT_MESSAGES_LAST_WEEK_MARKUP,
        parse_mode=ParseMode.HTML
    )


async def recent_messages_navigation_button(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    await query.answer()

    current_start_of_week: datetime = context.user_data['current_messages_week_start']
    if query.data == str(RECENT_MESSAGES_PREVIOUS_BUTTON):
        new_start_of_week = current_start_of_week - timedelta(days=6)
    elif query.data == str(RECENT_MESSAGES_NEXT_BUTTON):
        new_start_of_week = current_start_of_week + timedelta(days=6)
    elif query.data == str(RECENT_MESSAGES_CURRENT_BUTTON):
        new_start_of_week = get_current_week_start()
    else:
        new_start_of_week = current_start_of_week

    context.user_data['current_messages_week_start'] = new_start_of_week
    new_end_of_week = new_start_of_week + timedelta(days=6)
    period = (new_start_of_week, new_end_of_week)
    user_messages = await get_user_messages_by_period(update.effective_user.id, period)
    user_messages = f"{datetime.date(new_start_of_week)} — {datetime.date(new_end_of_week)}\n\n" + user_messages

    if new_start_of_week >= get_current_week_start():
        await query.edit_message_text(
            user_messages,
            reply_markup=SHOW_RECENT_MESSAGES_LAST_WEEK_MARKUP,
            parse_mode=ParseMode.HTML
        )
    else:
        await query.edit_message_text(
            user_messages,
            reply_markup=SHOW_RECENT_MESSAGES_MARKUP,
            parse_mode=ParseMode.HTML
        )


async def messages_navigation_button(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    await query.answer()

    current_page: int = context.user_data['current_messages_page']
    if query.data == str(MESSAGES_PREVIOUS_BUTTON):
        new_page = current_page - 1
    elif query.data == str(MESSAGES_NEXT_BUTTON):
        new_page = current_page + 1
    elif query.data == str(MESSAGES_FIRST_BUTTON):
        new_page = 1
    else:
        new_page = 1

    context.user_data['current_messages_page'] = new_page
    user_messages = await get_user_messages_by_page(update.effective_user.id, new_page)
    if new_page == 1:
        await query.edit_message_text(
            user_messages,
            reply_markup=SHOW_MESSAGES_FIRST_PAGE_MARKUP,
            parse_mode=ParseMode.HTML
        )
    else:
        await query.edit_message_text(
            user_messages,
            reply_markup=SHOW_MESSAGES_MARKUP,
            parse_mode=ParseMode.HTML
        )


async def show_messages(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user: User = User.get_or_none(tg_id=update.effective_user.id)
    if user is None or user.initial_setup_status != user.INITIAL_SETUP_DONE:
        await update.message.reply_text("You aren't logged in yet! Use /start or wait for your setup to complete")
        return

    context.user_data['current_messages_page'] = 1
    user_messages = await get_user_messages_by_page(update.effective_user.id, 1)

    await update.message.reply_text(
        user_messages,
        reply_markup=SHOW_MESSAGES_FIRST_PAGE_MARKUP,
        parse_mode=ParseMode.HTML
    )


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="/start - information about the bot\n"
             "/reset - reset your account, all the data will be deleted!\n"
             "/update - update your message list ahead of the 30 min interval. "
             "please, don't spam this command!\n"
             "/recent - show all the messages from the current week\n"
             "/messages - browse all your messages"
    )


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    # TODO: user filter exists
    already_logged_in_states = [
        User.INITIAL_SETUP_PENDING,
        User.INITIAL_SETUP_JUST_FINISHED,
        User.INITIAL_SETUP_DONE
    ]
    user: User | None = User.get_or_none(tg_id=update.effective_user.id)
    if user is None:
        # i hate markdown_v2
        greeting_message = ("Hello\!\nThis bot acts as a bridge between the notoriously laggy and unstable "
                            "bonch \(lk\.sut\.ru\) website and Telegram\.\n\n"
                            "Current features include:\n"
                            "1\) Messages and files browsing \(accessible even if the bonch website is down\)\n"
                            "2\) Notifications about new messages\n\n"
                            "IMPORTANT\!\!\!\n"
                            "Unfortunately, due to technical limitations, "
                            "your login data must be stored as plain text\. "
                            "If you use your main password for the bonch website, "
                            "it's advised to change it immediately "
                            "regardless of whether you're going to use this bot or not\. "
                            "To change the password, from the websites main menu go to *Personal*,"
                            " then *Change Password*\.\n\n"
                            "The bot is work in progress, you can request new features @obama\_xvatit")
        await update.message.reply_text(
            text=greeting_message,
            parse_mode=ParseMode.MARKDOWN_V2,
            reply_markup=PRE_SETUP_MARKUP
        )
        return PRE_SETUP
    if user.initial_setup_status in already_logged_in_states:
        await update.message.reply_text("You're already logged in. To reset your account use /reset")
        return ConversationHandler.END


async def ask_credentials(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text("Please, enter your login")
    context.user_data.clear()
    return TYPING_CREDENTIALS


async def save_credentials(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    user_data = context.user_data
    user_credential = update.effective_message.text

    if 'login' not in user_data:
        user_data['login'] = user_credential
        await update.message.reply_text("Please, enter your password")
        return TYPING_CREDENTIALS

    user_data['password'] = user_credential
    await update.message.reply_text(
        f"Your login is: {user_data['login']},"
        f" password is: {user_data['password']}. Is that correct?",
        reply_markup=CONFIRM_CREDENTIALS_MARKUP
    )
    return CREDENTIALS_SAVED


async def reset_account(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    notification_loop: NewMessagesNotificationLoop = context.bot_data["notification_loop"]
    user_id = update.effective_user.id
    user: User = User.get_or_none(tg_id=user_id)
    awaiting_setup_completion = [
        User.INITIAL_SETUP_PENDING,
        User.INITIAL_SETUP_JUST_FINISHED
    ]

    if user is None or user.initial_setup_status in awaiting_setup_completion:
        await update.message.reply_text("You aren't logged in yet! Use /start or wait for your setup to complete")

    elif (update.effective_message.text.split()[-1].lower() == 'confirm' and
          user.initial_setup_status not in awaiting_setup_completion):
        await update.message.reply_text("Deleting user data..")

        notification_loop.remove(user_id)
        user.delete_instance()

        logger.info(f"User {user.full_name}, {user.tg_id} reset their account")
        await update.message.reply_text(
            "Your account is successfully reset! "
            "You can start the login procedure using /start"
        )

    else:
        await update.message.reply_text(
            "If you want to reset your account, type: "
            "/reset confirm"
        )


# TODO: implement rate limiting
async def update_messages(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    worker_pool: WorkerPool = context.bot_data["worker_pool"]
    user: User = User.get_or_none(tg_id=update.effective_user.id)

    if user is None or user.initial_setup_status != user.INITIAL_SETUP_DONE:
        await update.message.reply_text("You aren't logged in yet! Use /start or wait for your setup to complete")
    else:
        await update.message.reply_text(
            "Checking for new messages.. "
            "You'll be notified if you have any in 15 seconds")
        logger.info(f"User {user.full_name} requested manual messages update")

        task = CheckNewMessages(user)
        worker_pool.add_task(task)

        context.job_queue.run_once(
            callback=new_messages_notify,
            when=15,
            chat_id=update.effective_chat.id,
            name=f"notify_{user.tg_id}",
            data=user.tg_id
        )


async def check_new_messages(context: CallbackContext):
    worker_pool: WorkerPool = context.bot_data["worker_pool"]
    user: User = User.get(tg_id=context.job.data)
    task = CheckNewMessages(user)
    worker_pool.add_task(task)


def start_bot(api_token: str) -> None:
    try:
        init_db()
    except peewee.OperationalError as e:
        logger.exception("Couldn't initiate database connection", exc_info=e)
        return

    logger.info("Successfully initiated the database")

    application = (Application.
                   builder().
                   token(api_token).
                   connect_timeout(20).
                   connection_pool_size(1024).
                   build()
                   )

    application.add_handler(CommandHandler("help", help_command))
    application.add_handler(CommandHandler('reset', reset_account))
    application.add_handler(CommandHandler('recent', show_recent_messages))
    application.add_handler(CommandHandler('update', update_messages))
    application.add_handler(CommandHandler('messages', show_messages))
    application.add_handler(CallbackQueryHandler(recent_messages_navigation_button, pattern="RECENT.*"))
    application.add_handler(CallbackQueryHandler(messages_navigation_button, pattern="MESSAGES.*"))

    start_handler = CommandHandler("start", start)
    conv_handler = ConversationHandler(
        entry_points=[start_handler],
        states={
            PRE_SETUP: [
                MessageHandler(
                    filters.Regex("^Start initial setup$") & ~filters.COMMAND, ask_credentials
                )
            ],
            TYPING_CREDENTIALS: [
                MessageHandler(
                    filters.TEXT & ~filters.COMMAND, save_credentials
                )
            ],
            CREDENTIALS_SAVED: [
                MessageHandler(
                    filters.Regex("^Confirm$") & ~filters.COMMAND, initial_setup
                ),
                MessageHandler(
                    filters.Regex("^Restart$") & ~filters.COMMAND, ask_credentials
                )
            ],
        },
        fallbacks=[start_handler],
    )
    application.add_handler(conv_handler)

    logger.info("Successfully added all the handlers")

    new_messages_notification_loop = NewMessagesNotificationLoop(
        application.job_queue,
        CHECK_NEW_MESSAGES_INTERVAL_SECONDS,
        NOTIFY_ABOUT_NEW_MESSAGES_INTERVAL_SECONDS
    )
    application.bot_data["notification_loop"] = new_messages_notification_loop

    logger.info("Successfully initialized new messages notification loop")

    worker_pool = WorkerPool(worker_count=4)
    application.bot_data["worker_pool"] = worker_pool

    logger.info("Successfully initialized worker pool")

    # Retry initial setup for users who started the procedure, but it never ended
    unfinished_setup_users: list[User]
    unfinished_setup_users = list((User.
                                  select().
                                  where(
                                    User.initial_setup_status != User.INITIAL_SETUP_DONE
                                  )
                                  ))
    logger.info(f"Found {len(unfinished_setup_users)} users with unfinished setup")
    for user in unfinished_setup_users:
        # TODO: avoid content duplication for users with unfinished setup
        if user.initial_setup_status == User.INITIAL_SETUP_PENDING:
            logger.info(f"Adding initial setup task for {user.full_name}")
            task = InitialSetupTask(user)
            worker_pool.add_task(task)

        logger.info(f"Will notify {user.full_name} on initial setup completion")
        application.job_queue.run_repeating(
            callback=initial_setup_notify,
            first=1,
            interval=20,
            chat_id=user.tg_id,
            name=f"notify_initial_setup_{user.tg_id}",
            data=user.tg_id
        )

    logged_in_users = User.select().where(User.initial_setup_status == User.INITIAL_SETUP_DONE)
    logger.info(f"Found {len(logged_in_users)} logged in users, adding them to the notification loop")
    for user in logged_in_users:
        new_messages_notification_loop.add(user.tg_id)

    new_messages_notification_loop.start()

    application.post_shutdown = stop_workers

    application.run_polling(allowed_updates=Update.ALL_TYPES)


async def stop_workers(application: Application) -> None:
    logger.info("Attempting to stop workers..")

    await application.job_queue.stop(wait=False)

    worker_pool: WorkerPool = application.bot_data["worker_pool"]
    worker_pool.stop()
