from telegram import ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup

PRE_SETUP_REPLY_KEYBOARD = [
    ["Start initial setup"],
    ["Close menu"]
]
PRE_SETUP_MARKUP = ReplyKeyboardMarkup(
    PRE_SETUP_REPLY_KEYBOARD,
    one_time_keyboard=True,
    resize_keyboard=True,
    is_persistent=False
)

CONFIRM_CREDENTIALS_REPLY_KEYBOARD = [
    ["Confirm"],
    ["Restart"]
]
CONFIRM_CREDENTIALS_MARKUP = ReplyKeyboardMarkup(
    CONFIRM_CREDENTIALS_REPLY_KEYBOARD,
    one_time_keyboard=True,
    resize_keyboard=True,
    is_persistent=False
)

RECENT_MESSAGES_PREVIOUS_BUTTON, RECENT_MESSAGES_NEXT_BUTTON, RECENT_MESSAGES_CURRENT_BUTTON = (
    "RECENT_MESSAGES_PREVIOUS", "RECENT_MESSAGES_NEXT", "RECENT_MESSAGES_CURRENT")

MESSAGES_PREVIOUS_BUTTON, MESSAGES_NEXT_BUTTON, MESSAGES_FIRST_BUTTON = (
    "MESSAGES_PREVIOUS", "MESSAGES_NEXT", "MESSAGES_CURRENT")

SHOW_RECENT_MESSAGES_KEYBOARD = [
    [
        InlineKeyboardButton("Current week", callback_data=RECENT_MESSAGES_CURRENT_BUTTON),
    ],
    [
        InlineKeyboardButton("Previous week", callback_data=RECENT_MESSAGES_PREVIOUS_BUTTON),
        InlineKeyboardButton("Next week", callback_data=RECENT_MESSAGES_NEXT_BUTTON),

    ],
]
SHOW_RECENT_MESSAGES_MARKUP = InlineKeyboardMarkup(SHOW_RECENT_MESSAGES_KEYBOARD)

SHOW_RECENT_MESSAGES_LAST_WEEK_KEYBOARD = [
    [
        InlineKeyboardButton("Previous week", callback_data=RECENT_MESSAGES_PREVIOUS_BUTTON),
    ],
]
SHOW_RECENT_MESSAGES_LAST_WEEK_MARKUP = InlineKeyboardMarkup(SHOW_RECENT_MESSAGES_LAST_WEEK_KEYBOARD)

ITEMS_PER_PAGE = 6

SHOW_MESSAGES_KEYBOARD = [
    [
        InlineKeyboardButton("First page", callback_data=MESSAGES_FIRST_BUTTON),
    ],
    [
        InlineKeyboardButton("Previous page", callback_data=MESSAGES_PREVIOUS_BUTTON),
        InlineKeyboardButton("Next page", callback_data=MESSAGES_NEXT_BUTTON),
    ],
]
SHOW_MESSAGES_MARKUP = InlineKeyboardMarkup(SHOW_MESSAGES_KEYBOARD)

SHOW_MESSAGES_FIRST_PAGE_KEYBOARD = [
    [
        InlineKeyboardButton("Next page", callback_data=MESSAGES_NEXT_BUTTON),
    ],
]
SHOW_MESSAGES_FIRST_PAGE_MARKUP = InlineKeyboardMarkup(SHOW_MESSAGES_FIRST_PAGE_KEYBOARD)