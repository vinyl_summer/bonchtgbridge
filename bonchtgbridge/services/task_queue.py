import logging
import threading
import queue

from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException

from bonchtgbridge.services.bonch_parser import InitialSetupTask, CheckNewMessages, StopThreadTask, AuthDataException
from bonchtgbridge.services.bonch_parser import perform_initial_setup, check_new_messages


logger = logging.getLogger(__name__)


def worker_target_method(worker_pool):
    driver_options = webdriver.FirefoxOptions()
    driver_options.add_argument('-headless')
    driver = webdriver.Firefox(options=driver_options)
    while True:
        task = worker_pool.queue.get()

        if isinstance(task, InitialSetupTask):
            user = task.user
            try:
                perform_initial_setup(user, driver)
            except AuthDataException:
                user.initial_setup_status = user.INITIAL_SETUP_AUTH_EXCEPTION
                user.save()
                logger.info(f"User {user.login} has invalid login data")
                continue
            except (TimeoutException, TimeoutError, NoSuchElementException):
                user.initial_setup_status = user.INITIAL_SETUP_TIMEOUT_EXCEPTION
                user.save()
                logger.info(f"Webdriver timeout while trying to log in {user.login}")
                continue
            user.initial_setup_status = user.INITIAL_SETUP_JUST_FINISHED
            user.save()

        elif isinstance(task, CheckNewMessages):
            user = task.user
            try:
                check_new_messages(driver, user)
            except (TimeoutException, TimeoutError, NoSuchElementException):
                logger.info(f'Webdriver timeout exception when trying to check {user.login} new messages')

        elif isinstance(task, StopThreadTask):
            driver.quit()
            logger.info('Worker stopped, webdriver successfully closed')
            worker_pool.queue.task_done()
            break

        worker_pool.queue.task_done()


class WorkerPool:
    def __init__(self, worker_count=4) -> None:
        self.worker_count = worker_count
        self.worker_threads = []
        self.queue = queue.Queue()
        for _ in range(self.worker_count):
            self.worker_threads.append(
                threading.Thread(
                    target=worker_target_method,
                    args=(self,),
                    daemon=False
                ).start()
            )

    def add_task(self, task) -> None:
        self.queue.put(task)

    def stop(self) -> None:
        for _ in range(self.worker_count):
            task = StopThreadTask()
            self.queue.put(task)
        self.queue.join()
