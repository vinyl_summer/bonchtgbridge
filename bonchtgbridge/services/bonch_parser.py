import datetime
import logging
import re

from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.expected_conditions import (invisibility_of_element_located,
                                                            element_to_be_clickable)
from selenium.common.exceptions import UnexpectedAlertPresentException, TimeoutException
from bs4 import BeautifulSoup, NavigableString

from bonchtgbridge.models.models import Content, User


DRIVER_TIMEOUT = 25


logger = logging.getLogger(__name__)


class WorkerTask:
    pass


class InitialSetupTask(WorkerTask):
    user: User

    def __init__(self, user: User):
        self.user = user


class CheckNewMessages(WorkerTask):
    user: User

    def __init__(self, user: User):
        self.user = user


class StopThreadTask(WorkerTask):
    pass


class AuthDataException(Exception):
    pass


def _authenticate(driver: Firefox, user: User) -> None:
    driver.delete_all_cookies()
    driver.implicitly_wait(DRIVER_TIMEOUT)
    driver.get("https://lk.sut.ru/cabinet/")
    if not driver.title.startswith('sut.RU - bonch'):
        raise TimeoutException

    email_box = driver.find_element(by=By.ID, value='users')
    password_box = driver.find_element(by=By.ID, value='parole')
    submit_button = driver.find_element(by=By.ID, value="logButton")

    email_box.send_keys(user.login)
    password_box.send_keys(user.password)

    submit_button.click()

    try:
        WebDriverWait(driver, timeout=DRIVER_TIMEOUT).until(invisibility_of_element_located(submit_button))
        header = driver.find_element(by=By.TAG_NAME, value='h2')
        if not header.text == "Новости СПБГУТ":
            raise TimeoutException
    except UnexpectedAlertPresentException:
        raise AuthDataException("Invalid login data")


def _is_message_row(message_id) -> bool:
    return message_id and re.compile("^tr_|^show_").search(message_id)


def _next_page_is_present(nav_buttons) -> bool:
    for button in nav_buttons:
        if button.text == ' Следующая ':
            return True
    return False


def _process_date_time(date_time_string) -> datetime.datetime:
    date, time = date_time_string.split()
    day, month, year = date.split("-")
    hour, minute, second = time.split(":")
    return datetime.datetime(int(year), int(month), int(day), int(hour), int(minute), int(second))


# Remember: never troubleshoot a selenium function without -headless, if you're going to use it with -headless
def _flip_content_page(web_driver: Firefox, next_button) -> None:
    next_button = web_driver.find_element(by=By.CSS_SELECTOR, value=next_button)
    web_driver.find_element(by=By.CSS_SELECTOR, value='#rightpanel').send_keys(Keys.CONTROL, Keys.END)
    web_driver.implicitly_wait(DRIVER_TIMEOUT)
    next_button.click()


def _save_all_user_data(driver: Firefox, user: User) -> None:
    full_name = driver.find_element(by=By.CSS_SELECTOR, value='#info').text.split('\n')[1]
    user.full_name = full_name
    user.save()

    ''' Get all the messages of the user'''
    # TODO: change ids for smth else cause they are auto generated
    messages_menu = driver.find_element(by=By.ID, value='menu_li_840')
    messages_menu.click()

    driver.implicitly_wait(DRIVER_TIMEOUT)
    header = driver.find_element(by=By.TAG_NAME, value='h2')
    if not header.text == "Мои сообщения":
        raise TimeoutException

    old_first_row = None
    # fake do while loop, because I need to parse a page first and then check if there is a next one
    while True:
        if old_first_row is not None:
            WebDriverWait(driver, timeout=DRIVER_TIMEOUT).until(invisibility_of_element_located(old_first_row))

        messages_page = BeautifulSoup(driver.page_source, 'html.parser')
        messages_rows = messages_page.find("table", attrs={'id': 'mytable'}).find("tbody").find_all("tr",
                                                                                                    id=_is_message_row)
        # Click all the rows to reveal the texts of the messages
        for i in range(0, len(messages_rows), 2):
            header_row = messages_rows[i]
            row_id = re.compile("tr_[0-9]+").findall(str(header_row))[0]
            driver.implicitly_wait(DRIVER_TIMEOUT)
            reveal_message_css_selector = f"#{row_id} > td:nth-child(2)"
            reveal_message = driver.find_element(by=By.CSS_SELECTOR, value=reveal_message_css_selector)
            reveal_message.click()

        messages_page = BeautifulSoup(driver.page_source, 'html.parser')
        messages_rows = messages_page.find("table", attrs={'id': 'mytable'}).find("tbody").find_all("tr",
                                                                                                    id=_is_message_row)

        # Iterate only through odd rows (first, third, e.t.c.), because a message takes up two rows
        for i in range(0, len(messages_rows), 2):
            header_row = messages_rows[i]
            header_columns = header_row.find_all('td')
            if len(header_columns) == 5:
                date_time_column, message_topic_column, files_column, sender_column, _ = header_columns
                date_time, message_topic, sender = (_process_date_time(date_time_column.text.strip()),
                                                    message_topic_column.text.strip(),
                                                    sender_column.text.strip().split('(')[0])
                files = list()
                for file in files_column:
                    if isinstance(file, NavigableString):
                        continue
                    files.append(file.get('href'))
                files_string = '\n'.join(files)
            else:
                continue

            message_row = messages_rows[i + 1]
            message = message_row.find('tr').find('td', attrs={'colspan': '2'}).find('span')
            message_text_list = message.find_all(string=re.compile('[a-zA-Zа-яА-Я0-9]+'), limit=None)
            message_text_list = [m.string for m in message_text_list]

            message_text = ' '.join(message_text_list)
            message_text = message_text.replace('\xa0', ' ')
            message = Content(type='message', date_time=date_time, topic=message_topic,
                              text=message_text, files=files_string, sender=sender, recipient=user, is_read=True)
            message.save()

        navigation_buttons = messages_page.find('span', attrs={'id': 'table_mes'}).find_all(
            'a', attrs={'href': None, 'name': None})
        next_button_css_selector = f'#table_mes > center:nth-child(5) > a:nth-child({len(navigation_buttons) + 1}'

        # Break condition
        if not _next_page_is_present(navigation_buttons):
            break

        old_first_row = driver.find_element(By.XPATH, "//tr[starts-with(@id, 'tr_')]")

        _flip_content_page(driver, next_button_css_selector)

    # Go back to the main menu
    driver.refresh()
    driver.implicitly_wait(DRIVER_TIMEOUT)

    ''' Get all the files of the user'''

    left_menu = driver.find_element(by=By.CLASS_NAME, value='title_item')
    left_menu.click()

    driver.implicitly_wait(DRIVER_TIMEOUT)
    files_menu = driver.find_element(by=By.ID, value='menu_li_842')
    WebDriverWait(driver, timeout=DRIVER_TIMEOUT).until(element_to_be_clickable(files_menu))
    files_menu.click()

    driver.implicitly_wait(DRIVER_TIMEOUT)
    header = driver.find_element(by=By.TAG_NAME, value='h2')
    if not header.text == "Файлы группы":
        raise TimeoutException

    old_first_row = None
    # fake do while loop is used because I need to parse a page first, then check if there is a next one
    while True:
        if old_first_row is not None:
            WebDriverWait(driver, timeout=DRIVER_TIMEOUT).until(invisibility_of_element_located(old_first_row))

        files_page = BeautifulSoup(driver.page_source, 'html.parser')
        files_rows = files_page.find("table", attrs={'id': 'mytable'}).find("tbody").find_all("tr",
                                                                                              id=_is_message_row)
        for row in files_rows:
            columns = row.find_all('td')
            if len(columns) == 8:
                _, sender_column, date_time_column, message_topic_column, message_column, files_column, _, _ = columns
                files_column = files_column.find_all('a')
                sender, date_time, message_topic, message_text = (
                    sender_column.text.strip().split('(')[0],
                    _process_date_time(date_time_column.text.strip()),
                    message_topic_column.text.strip(),
                    message_column.text.strip())
                files = list()
                for file in files_column:
                    if isinstance(file, NavigableString) or file is None:
                        continue
                    files.append(file.get('href'))
                files_string = '\n'.join(files)
            else:
                continue

            file = Content(type='file', date_time=date_time, topic=message_topic, text=message_text,
                           files=files_string, sender=sender, recipient=user, is_read=True)
            file.save()

            row_id = re.compile("tr_[0-9]+").findall(str(row))[0]
            read_message_css_selector = f"#{row_id} > td:nth-child(2)"
            read_message = driver.find_element(by=By.CSS_SELECTOR, value=read_message_css_selector)
            read_message.click()

        navigation_buttons = files_page.find('span', attrs={'id': 'table_mes'}).find_all(
            'a', attrs={'href': None, 'name': None})
        next_button_css_selector = f'#table_mes > a:nth-child({len(navigation_buttons) + 1})'

        # Break condition
        if not _next_page_is_present(navigation_buttons):
            break

        old_first_row = driver.find_element(By.XPATH, "//tr[starts-with(@id, 'tr_')]")

        _flip_content_page(driver, next_button_css_selector)


def check_new_messages(driver: Firefox, user: User) -> None:
    _authenticate(driver, user)
    main_menu_page = BeautifulSoup(driver.page_source, 'html.parser')

    try:
        notification = (main_menu_page.find(
            name='div',
            attrs={'id': 'rightpanel'})
                .find(
                    name='div',
                    attrs={'role': 'alert'}))
        new_messages_number = notification.find('b').string
        logger.info(f"User {user.login} got {new_messages_number} new messages, adding them to the database..")

        messages_menu = driver.find_element(by=By.ID, value='menu_li_840')
        messages_menu.click()

        driver.implicitly_wait(DRIVER_TIMEOUT)
        header = driver.find_element(by=By.TAG_NAME, value='h2')
        if not header.text == "Мои сообщения":
            raise TimeoutException

        messages_menu_page = BeautifulSoup(driver.page_source, 'html.parser')

        new_messages_rows_headers = (messages_menu_page.find(
            name="table",
            attrs={'id': 'mytable'})
                .find("tbody").find_all(
                    "tr",
                    id=_is_message_row,
                    attrs={"style": "font-weight: bold !important;"}))
        messages_rows_ids = list()
        new_messages = dict()
        # Click all the rows to reveal the texts of the messages and get the headers info
        for header_row in new_messages_rows_headers:
            header_columns = header_row.find_all('td')
            if len(header_columns) == 5:
                date_time_column, message_topic_column, files_column, sender_column, _ = header_columns
                date_time, message_topic, sender = (_process_date_time(date_time_column.text.strip()),
                                                    message_topic_column.text.strip(), sender_column.text.strip())
                files = list()
                for file in files_column:
                    if isinstance(file, NavigableString):
                        continue
                    files.append(file.get('href'))
                files_string = '\n'.join(files)
            else:
                continue

            row_id = re.compile("tr_[0-9]+").findall(str(header_row))[0][3:]
            reveal_message_css_selector = f"#tr_{row_id} > td:nth-child(2)"
            reveal_message = driver.find_element(by=By.CSS_SELECTOR, value=reveal_message_css_selector)
            reveal_message.click()

            messages_rows_ids.append(f"{row_id}")
            # TODO: potentially fix None files_string
            new_messages[row_id] = [date_time, message_topic, sender, files_string]

        messages_menu_source = driver.page_source
        messages_menu_page = BeautifulSoup(messages_menu_source, 'html.parser')

        for row_id in messages_rows_ids:
            message_row = (messages_menu_page.find(
                name="table",
                attrs={'id': 'mytable'})
                    .find("tbody").find(
                        name='tr',
                        attrs={'id': f'show_{row_id}'}))
            message = message_row.find('tr').find('td', attrs={'colspan': '2'}).find('span')
            message_text_list = message.find_all(string=re.compile('[a-zA-Zа-яА-Я0-9]+'), limit=None)
            message_text_list = [m.string for m in message_text_list]
            message_text = ' '.join(message_text_list)
            message_text = message_text.replace('\xa0', ' ')

            date_time, message_topic, sender, files_string = new_messages.get(row_id)
            message = Content(type='message', date_time=date_time,
                              topic=message_topic, text=message_text,
                              files=files_string, sender=sender,
                              recipient=user, is_read=False)
            message.save()
        logger.info(f"Saved {new_messages_number} new messages for {user.login}")
    except AttributeError:
        logger.info(f"User {user.login} got no new messages, should still check for new files")

    # Go back to the main menu
    driver.refresh()
    driver.implicitly_wait(DRIVER_TIMEOUT)

    left_menu = driver.find_element(by=By.CLASS_NAME, value='title_item')
    left_menu.click()

    driver.implicitly_wait(DRIVER_TIMEOUT)
    files_menu = driver.find_element(by=By.ID, value='menu_li_842')
    WebDriverWait(driver, timeout=DRIVER_TIMEOUT).until(element_to_be_clickable(files_menu))
    files_menu.click()

    driver.implicitly_wait(DRIVER_TIMEOUT)
    header = driver.find_element(by=By.TAG_NAME, value='h2')
    if not header.text == "Файлы группы":
        raise TimeoutException

    files_page = BeautifulSoup(driver.page_source, 'html.parser')

    new_files_rows = (files_page.find(
        name="table",
        attrs={'id': 'mytable'}
    ).find("tbody").find_all(
            name="tr",
            id=_is_message_row,
            attrs={"style": "font-weight: bold;"}
    ))
    logger.info(f"User {user.login} got {len(new_files_rows)} new files")

    for row in new_files_rows:
        columns = row.find_all('td')
        if len(columns) == 8:
            _, sender_column, date_time_column, message_topic_column, message_column, files_column, _, _ = columns
            files_column = files_column.find_all('a')
            sender, date_time, message_topic, message_text = (
                sender_column.text.strip().split('(')[0],
                _process_date_time(date_time_column.text.strip()),
                message_topic_column.text.strip(),
                message_column.text.strip())
            files = list()
            for file in files_column:
                if isinstance(file, NavigableString) or file is None:
                    continue
                files.append(file.get('href'))
            files_string = '\n'.join(files)
        else:
            continue

        file = Content(type='file', date_time=date_time,
                       topic=message_topic, text=message_text,
                       files=files_string, sender=sender,
                       recipient=user, is_read=False)
        file.save()

        row_id = re.compile("tr_[0-9]+").findall(str(row))[0]
        read_message_css_selector = f"#{row_id} > td:nth-child(2)"
        read_message = driver.find_element(by=By.CSS_SELECTOR, value=read_message_css_selector)
        read_message.click()


def perform_initial_setup(user: User, driver: Firefox) -> None:
    user_login = user.login

    logger.info(f'Authenticating {user_login}')
    _authenticate(driver, user)
    logger.info(f'{user_login} successfully authenticated')

    logger.info(f'Saving {user_login} user data')
    _save_all_user_data(driver, user)
    logger.info(f'User {user_login} initial setup complete')
