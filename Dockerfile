FROM python:3.11-buster
RUN apt-get update && apt-get install -y firefox-esr
RUN cd /usr/bin && wget https://github.com/mozilla/geckodriver/releases/download/v0.34.0/geckodriver-v0.34.0-linux64.tar.gz && tar xvf geckodriver-v0.34.0-linux64.tar.gz
ENV PYTHONBUFFERED=1
RUN mkdir /app
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
CMD python -m bonchthbridge
